import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {IdeaService} from '../../../services/idea.service';
import {finalize} from 'rxjs/operators';
import {Goal} from '../../../model/goal';

@Component({
  selector: 'app-create-idea',
  templateUrl: './create-idea.component.html',
  styleUrls: ['./create-idea.component.scss']
})
export class CreateIdeaComponent implements OnInit {

  form: FormGroup;
  goals: Goal[] = [];
  loading: boolean = false;
  loadingError: any | null = null;
  saving: boolean = false;
  savingError: any | null = null;
  submitted: boolean = false;
  success: boolean = false;

  constructor(private fb: FormBuilder,
              private ideaService: IdeaService) {
    this.form = this.fb.group({
      title: [null, Validators.required],
      submitterName: [null],
      description: [null, Validators.required],
      imageUrl: [null, this.isValidUrl],
    });
  }

  ngOnInit(): void {
  }

  save() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    const request = Object.assign({}, this.form.value);
    this.ideaService.create(request)
      .pipe(finalize(() => this.saving = false))
      .subscribe(
        () => this.success = true,
        error => this.savingError = error
      )
  }

  isValidUrl: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const url = control.value;
    if (!url?.length) {
      return null;
    }
    // Validate image extension
    const imageExtensions = ['png', 'jpg', 'jpg', 'svg', 'gif'];
    const extension = url.substr(url.length - 3, 3);
    if (imageExtensions.indexOf(extension) == -1) {
      return {invalidUrl: true};
    }
    // Validate url
    try {
      const test = new URL(url);
    } catch (e) {
      return {invalidUrl: true};
    }
    return null;
  }
}
