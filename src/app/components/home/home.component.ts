import {Component, OnInit} from '@angular/core';
import {IdeaService} from "../../services/idea.service";
import {Idea} from "../../model/idea";
import {debounceTime, distinctUntilChanged, finalize} from "rxjs/operators";
import {faSearch, faSpinner} from '@fortawesome/free-solid-svg-icons';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Goal} from "../../model/goal";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  spinnerIcon = faSpinner;
  searchIcon = faSearch;

  searchForm: FormGroup;
  goals: Goal[] = [];
  loading: boolean = false;
  loadingError: any = null;
  ideas: Idea[] = [];
  searching: boolean = false;
  searchingError: any = null;

  constructor(private ideaService: IdeaService,
              private formBuilder: FormBuilder) {
    this.searchForm = this.formBuilder.group({
      searchTerm: [''],
      goal: ['']
    });
  }

  ngOnInit(): void {
    this.setupSearchForm();
    this.search();
  }

  private setupSearchForm() {
    // Perform a search shortly after any distinct value change is detected
    this.searchForm.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged()
      )
      .subscribe(() => this.search());
  }

  search() {
    this.loading = true;
    this.ideaService.search(this.searchForm.value)
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        ideas => this.ideas = ideas,
        error => this.loadingError = error
      );
  }
}
